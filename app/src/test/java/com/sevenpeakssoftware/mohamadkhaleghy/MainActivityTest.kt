package com.sevenpeakssoftware.mohamadkhaleghy

import com.sevenpeakssoftware.mohamadkhaleghy.tools.SIMPLE_TEST_TITLE
import com.sevenpeakssoftware.mohamadkhaleghy.tools.SimpleTestFragment
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.AutoCloseKoinTest
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class MainActivityTest : AutoCloseKoinTest() {

    @Test
    fun `open a simple page`() {
        val pageTag = "simplePage"

        MainActivityTestRobot()
            .openPage(SimpleTestFragment.SimpleTestPage(pageTag))
            .assertPageExist(pageTag)

    }

    @Test
    fun `open a simple page and added to backStack`() {
        val pageTag = "simplePage"
        MainActivityTestRobot()
            .openPage(SimpleTestFragment.SimpleTestPage(pageTag, addedToBackStack = true))
            .assertPageExist(pageTag)
            .isAddedToBackStack(1)
    }

    @Test
    fun `check the title of page`() {
        val pageTag = "simplePage"
        MainActivityTestRobot()
            .openPage(SimpleTestFragment.SimpleTestPage(pageTag, addedToBackStack = true))
            .assertTitle(SIMPLE_TEST_TITLE)
    }

}