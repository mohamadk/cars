package com.sevenpeakssoftware.mohamadkhaleghy

import com.sevenpeakssoftware.mohamadkhaleghy.tools.SimpleTestFragment
import org.junit.Assert
import org.robolectric.Robolectric
import org.robolectric.android.controller.ActivityController

class MainActivityTestRobot {

    private val controller: ActivityController<MainActivity> =
        Robolectric.buildActivity(MainActivity::class.java).setup()

    fun openPage(
        page: SimpleTestFragment.SimpleTestPage
    ): MainActivityTestRobot {
        controller.get().open(page)
        return this
    }

    fun assertPageExist(tag: String): MainActivityTestRobot {
        Assert.assertNotNull(controller.get().supportFragmentManager.findFragmentByTag(tag))
        return this
    }

    fun isAddedToBackStack(backStackEntryCount: Int): MainActivityTestRobot {
        Assert.assertEquals(controller.get().supportFragmentManager.backStackEntryCount, backStackEntryCount)
        return this
    }

    fun assertTitle(title: String): MainActivityTestRobot {
        Assert.assertEquals(controller.get().title, title)
        return this
    }


}
