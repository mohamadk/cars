package com.sevenpeakssoftware.mohamadkhaleghy.tools

import android.os.Bundle
import com.mohamadk.pagingfragment.base.BaseFragmentOpenerFragment
import com.mohamadk.pagingfragment.base.factories.PageFactory

const val SIMPLE_TEST_TITLE = "SimplePage"

class SimpleTestFragment : BaseFragmentOpenerFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        titlePage = SIMPLE_TEST_TITLE
    }


    class SimpleTestPage(private val tag: String, private val addedToBackStack: Boolean = false) :
        PageFactory(SimpleTestFragment::class.java) {
        override fun tag(): String? {
            return tag
        }

        override fun addToBackStack(): Boolean {
            return addedToBackStack
        }

    }

}