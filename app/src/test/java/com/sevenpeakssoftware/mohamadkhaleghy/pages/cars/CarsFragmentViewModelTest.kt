package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.mohamadk.middleman.networkstate.NetworkState
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.test.AutoCloseKoinTest
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class CarsFragmentViewModelTest : AutoCloseKoinTest() {

    @JvmField
    @Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun `post Items And convert Server Date to Client Date format, different year`() {

        val cars = listOf(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )

        )
        val formattedCars = listOf(
            Car(
                1, "title", "ingress", "imageurl"

                , "25 May 2018, 02:13 PM"

                , listOf(), listOf(), 1, 1
            )
        )

        CarsFragmentViewModelTestRobot()
            .postItems(cars)
            .checkResult(formattedCars)
    }

    @Test
    fun `post Items And convert Server Date to Client Date format, current year`() {

        val cars = listOf(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2019 14:13"

                , listOf(), listOf(), 1, 1
            )

        )
        val formattedCars = listOf(
            Car(
                1, "title", "ingress", "imageurl"

                , "25 May, 02:13 PM"

                , listOf(), listOf(), 1, 1
            )
        )

        CarsFragmentViewModelTestRobot()
            .postItems(cars)
            .checkResult(formattedCars)
    }

    @Test
    fun `post networkState to refreshState`() {
        CarsFragmentViewModelTestRobot()
            .postNetWorkState(NetworkState.LOADING)
            .checkRefreshState(NetworkState.LOADING)
    }

    @Test
    fun `call retry`() {

        CarsFragmentViewModelTestRobot()
            .callRetry()
            .checkIfRetryCalledInRepo()

    }


}