package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.mohamadk.middleman.networkstate.NetworkState
import com.nhaarman.mockitokotlin2.*
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.db.CarsDao
import io.reactivex.Observable
import junit.framework.TestCase.assertEquals

class CarsRepositoryImplTestRobot(
    val dbResult: MutableLiveData<List<Car>>,
    apiResult: Observable<Array<Car>>
) {

    private val carsDao: CarsDao = mock()
    private val carsApi: CarsApi = mock()
    private val carsRepositoryImpl: CarsRepositoryImpl

    private val carsObserver: Observer<List<Car>> = mock()
    private val refreshStateObserver: Observer<NetworkState> = mock()

    init {

        whenever(carsDao.loadCars()).thenReturn(dbResult)
        whenever(carsApi.loadCars()).thenReturn(apiResult)

        carsRepositoryImpl = CarsRepositoryImpl(carsDao, carsApi)

        carsRepositoryImpl.carsListLive.observeForever(carsObserver)
        carsRepositoryImpl.refreshStateLive.observeForever(refreshStateObserver)

    }


    fun loadItems(): CarsRepositoryImplTestRobot {
        carsRepositoryImpl.loadItems()
        Thread.sleep(500)
        return this
    }

    fun checkRefreshState(vararg networkStates: NetworkState) {

        val networkStateCapture = argumentCaptor<NetworkState>()

        verify(refreshStateObserver, times(networkStates.size)).onChanged(networkStateCapture.capture())

        assertEquals(networkStates.toList(), networkStateCapture.allValues)
    }

    fun checkIfApiCalled(): CarsRepositoryImplTestRobot {
        verify(carsApi, times(1)).loadCars()
        return this
    }

    fun checkIfDBSaveItemsCalled(cars: List<Car>): CarsRepositoryImplTestRobot {
        verify(carsDao, times(1)).saveCars(cars)
        return this
    }

}
