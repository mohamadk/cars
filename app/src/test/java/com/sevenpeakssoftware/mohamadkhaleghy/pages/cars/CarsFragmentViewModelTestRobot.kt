package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.mohamadk.middleman.networkstate.NetworkState
import com.nhaarman.mockitokotlin2.*
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.CarsRepository
import junit.framework.TestCase.assertEquals


class CarsFragmentViewModelTestRobot {

    private val carsRepository: CarsRepository = mock()
    private var carsFragmentViewModel: CarsFragmentViewModel

    private val itemsLive = MutableLiveData<List<Car>>()
    private val refreshStateLive = MutableLiveData<NetworkState>()

    private val itemsObserver: Observer<List<Car>> = mock()
    private val refreshStateObserver: Observer<NetworkState> = mock()

    init {
        whenever(carsRepository.carsListLive).thenReturn(itemsLive)
        whenever(carsRepository.refreshStateLive).thenReturn(refreshStateLive)

        carsFragmentViewModel = CarsFragmentViewModel(carsRepository)

        carsFragmentViewModel.items.observeForever(itemsObserver)
        carsFragmentViewModel.refreshState.observeForever(refreshStateObserver)

    }

    fun postItems(items: List<Car>): CarsFragmentViewModelTestRobot {
        itemsLive.value = items
        return this
    }

    fun checkResult(vararg cars: List<Car>): CarsFragmentViewModelTestRobot {

        val formattedCarsCapture = argumentCaptor<List<Car>>()
        verify(itemsObserver, times(cars.size)).onChanged(formattedCarsCapture.capture())

        assertEquals(cars.toList(), formattedCarsCapture.allValues)

        return this
    }

    fun postNetWorkState(loading: NetworkState): CarsFragmentViewModelTestRobot {
        refreshStateLive.value = loading
        return this
    }

    fun checkRefreshState(vararg networkStates: NetworkState): CarsFragmentViewModelTestRobot {
        val networkStateCapture = argumentCaptor<NetworkState>()

        verify(refreshStateObserver, times(1)).onChanged(networkStateCapture.capture())

        assertEquals(networkStates.toList(), networkStateCapture.allValues)

        return this
    }

    fun callRetry(): CarsFragmentViewModelTestRobot {
        carsFragmentViewModel.retry()
        return this
    }

    fun checkIfRetryCalledInRepo() {
        verify(carsRepository, times(1)).retry()
    }


}
