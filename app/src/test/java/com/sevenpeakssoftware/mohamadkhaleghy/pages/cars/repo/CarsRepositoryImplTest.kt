package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.mohamadk.middleman.networkstate.NetworkState
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import io.reactivex.Observable
import org.junit.Rule
import org.junit.Test

class CarsRepositoryImplTest {

    @JvmField
    @Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()


    @Test
    fun `check network state sequence for success result`() {

        val serverCars = arrayOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )
        val dbCars = listOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )

        CarsRepositoryImplTestRobot(
            dbResult = MutableLiveData<List<Car>>()
                .apply {
                    value = dbCars
                }
            , apiResult = Observable.just(serverCars)
        )
            .loadItems()
            .checkRefreshState(
                NetworkState.LOADING, NetworkState.LOADED
            )
    }

    @Test
    fun `check network state sequence for failure result`() {

        val dbCars = listOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )
        val errorMessage = "something went wrong :O"

        CarsRepositoryImplTestRobot(
            dbResult = MutableLiveData<List<Car>>()
                .apply {
                    value = dbCars
                }
            , apiResult = Observable.error(IllegalStateException(errorMessage))
        )
            .loadItems()
            .checkRefreshState(
                NetworkState.LOADING, NetworkState.error(errorMessage)
            )

    }

    @Test
    fun `check api called`() {
        val serverCars = arrayOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )
        val dbCars = listOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )

        CarsRepositoryImplTestRobot(
            dbResult = MutableLiveData<List<Car>>()
                .apply {
                    value = dbCars
                }
            , apiResult = Observable.just(serverCars)
        )

            .loadItems()
            .checkIfApiCalled()
    }

    @Test
    fun `check db save items called`() {

        val serverCars = listOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )
        val dbCars = listOf<Car>(
            Car(
                1, "title", "ingress", "imageurl"

                , "25.05.2018 14:13"

                , listOf(), listOf(), 1, 1
            )
        )

        CarsRepositoryImplTestRobot(
            dbResult = MutableLiveData<List<Car>>()
                .apply {
                    value = dbCars
                }
            , apiResult = Observable.just(serverCars.toTypedArray())
        )

            .loadItems()
            .checkIfDBSaveItemsCalled(serverCars)
    }


}