package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item

import com.google.gson.annotations.SerializedName

data class Item(
    @SerializedName("type")
    val type: String
    , @SerializedName("subject")
    val subject: String
    , @SerializedName("description")
    val description: String
)