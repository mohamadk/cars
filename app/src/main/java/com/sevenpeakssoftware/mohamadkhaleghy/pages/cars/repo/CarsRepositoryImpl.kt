package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo

import androidx.lifecycle.LiveData
import com.mohamadk.middleman.networkstate.NetworkState
import com.sevenpeakssoftware.mohamadkhaleghy.core.utils.extentions.handleConnectionErrors
import com.sevenpeakssoftware.mohamadkhaleghy.core.utils.extentions.toLiveData
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.db.CarsDao
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject

class CarsRepositoryImpl(
    carsDao: CarsDao
    , carsApi: CarsApi

) : CarsRepository {

    private val loadCarsSubject = BehaviorSubject.create<Long>()

    override val refreshStateLive: LiveData<NetworkState> = loadCarsSubject
        .flatMap {
            carsApi.loadCars()
                .doOnNext { cars ->
                    carsDao.saveCars(cars.toList())
                }
                .map {
                    NetworkState.LOADED
                }
                .onErrorReturn(handleConnectionErrors())
                .subscribeOn(Schedulers.io())
                .startWith(NetworkState.LOADING)
        }.toLiveData()

    override val carsListLive: LiveData<List<Car>> = carsDao.loadCars()

    override fun loadItems() {
        loadCarsSubject.onNext(1)
    }

    override fun retry() {
        loadItems()
    }


}