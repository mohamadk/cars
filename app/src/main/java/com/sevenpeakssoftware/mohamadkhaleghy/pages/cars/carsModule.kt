package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars

import com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db.CarsDB
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.CarsApi
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.CarsRepository
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.CarsRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val carsModule = module {

    viewModel {
        CarsFragmentViewModel(carsRepository = get())
    }

    single<CarsRepository> {
        CarsRepositoryImpl(carsDao = get(), carsApi = get())
    }

    single {
        this.get<CarsDB>().carsDao
    }

    single<CarsApi> {
        val retrofit: Retrofit = get()
        retrofit.create(CarsApi::class.java)
    }
}