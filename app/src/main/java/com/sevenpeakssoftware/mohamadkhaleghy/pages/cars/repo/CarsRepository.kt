package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo

import androidx.lifecycle.LiveData
import com.mohamadk.middleman.networkstate.NetworkState
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car

interface CarsRepository {

    val refreshStateLive: LiveData<NetworkState>
    val carsListLive: LiveData<List<Car>>

    fun loadItems()
    fun retry()
}
