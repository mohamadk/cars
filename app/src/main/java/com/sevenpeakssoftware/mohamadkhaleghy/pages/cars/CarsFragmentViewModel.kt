package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars


import android.text.format.DateFormat
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.mohamadk.middleman.networkstate.NetworkState
import com.mohamadk.pagingfragment.base.list.BaseListFragmentViewModel
import com.sevenpeakssoftware.mohamadkhaleghy.core.App
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.CarsRepository
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CarsFragmentViewModel(private val carsRepository: CarsRepository) : BaseListFragmentViewModel<List<Car>>() {
    private val serverDateTimeFormat = SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH)
    var isFirstLoad = true

    override val refreshState: LiveData<NetworkState> = carsRepository.refreshStateLive

    override val items: LiveData<List<Car>> = Transformations.map(carsRepository.carsListLive) { cars ->
        cars.forEach {
            it.dateTime = convertServerTimeFormatToClientFormat(it.dateTime)
        }
        cars
    }

    /**
     * convert                       current year           12H format
     * 29.11.2017 15:11              29.11.2019 15:11       29.11.2019 15:11
     * to
     * 29 November 2017, 15:11       29 November, 15:11     29 November, 03:11 PM
     */
    private fun convertServerTimeFormatToClientFormat(dateTime: String): String {
        return try {
            val calendar = Calendar.getInstance()
            calendar.time = serverDateTimeFormat.parse(dateTime)

            SimpleDateFormat(createClientDateTimeFormat(calendar), Locale.ENGLISH)
                .format(calendar.time)

        } catch (e: ParseException) {
            dateTime
        }
    }

    private fun createClientDateTimeFormat(calendar: Calendar): String {
        var clientDateTimeFormat = "dd MMMM yyyy, HH:mm"
        if (!DateFormat.is24HourFormat(App.context)) {
            clientDateTimeFormat = clientDateTimeFormat.replace("HH", "hh") + " a"
        }

        if (calendar.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR)) {
            clientDateTimeFormat = clientDateTimeFormat.replace(" yyyy", "")
        }
        return clientDateTimeFormat
    }

    override fun loadItems(vararg args: Any) {
        isFirstLoad = false
        carsRepository.loadItems()
    }

    override fun retry(vararg args: Any) {
        carsRepository.retry()
    }
}
