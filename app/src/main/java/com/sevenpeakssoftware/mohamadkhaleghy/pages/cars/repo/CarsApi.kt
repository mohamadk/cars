package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo

import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import io.reactivex.Observable
import retrofit2.http.GET

interface CarsApi {

    @GET("article/get_articles_list")
    fun loadCars(): Observable<Array<Car>>
}
