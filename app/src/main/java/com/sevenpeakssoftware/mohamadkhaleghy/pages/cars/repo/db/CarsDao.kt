package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car

@Dao
interface CarsDao {

    @Query("select * from Cars")
    fun loadCars(): LiveData<List<Car>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCars(cars: List<Car>)

}
