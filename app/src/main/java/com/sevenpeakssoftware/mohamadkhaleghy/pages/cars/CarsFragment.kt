package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars

import android.os.Bundle
import com.mohamadk.middleman.AdapterProvider
import com.mohamadk.middleman.adapter.GeneralViewAdapter
import com.mohamadk.pagingfragment.base.factories.PageFactory
import com.mohamadk.pagingfragment.base.list.BaseListFragment
import com.sevenpeakssoftware.mohamadkhaleghy.R
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import org.koin.androidx.viewmodel.ext.android.viewModel

class CarsFragment : BaseListFragment<List<Car>, CarsFragmentViewModel>() {

    override val viewModel: CarsFragmentViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        titlePage = getString(R.string.carsPage)

        if (viewModel.isFirstLoad) {
            loadItems()
        }

    }

    override fun provideAdapter(): AdapterProvider<List<Car>> {
        return GeneralViewAdapter(this)
    }

    override fun submitList(it: List<Car>?) {
        adapterProvider.submitList(it)
    }

    class CarsPage(private val addedToBackStack: Boolean = false) : PageFactory(CarsFragment::class.java) {
        override fun addToBackStack(): Boolean {
            return addedToBackStack
        }
    }


}