package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.mohamadk.middleman.model.BaseModel
import com.sevenpeakssoftware.mohamadkhaleghy.R

@Entity(tableName = "Cars")
data class Car(
    @PrimaryKey
    @SerializedName("id")
    val id: Long
    , @SerializedName("title")
    val title: String
    , @SerializedName("ingress")
    val ingress: String
    , @SerializedName("image")
    val image: String
    , @SerializedName("dateTime")
    var dateTime: String
    , @SerializedName("tags")
    val tags: List<String>
    , @SerializedName("content")
    val content: List<Item>
    , @SerializedName("created")
    val created: Long
    , @SerializedName("changed")
    val changed: Long
) : BaseModel {
    override fun defaultResLayout(position: Int): Int? {
        return R.layout.item_car
    }

    override fun defaultViewClass(position: Int): Class<*>? {
        return null
    }

}
