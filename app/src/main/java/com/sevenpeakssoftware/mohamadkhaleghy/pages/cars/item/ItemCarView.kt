package com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import com.mohamadk.middleman.Binder
import com.sevenpeakssoftware.mohamadkhaleghy.core.imageloder.GlideApp
import com.sevenpeakssoftware.mohamadkhaleghy.core.utils.extentions.kotlinNullHandler
import kotlinx.android.synthetic.main.item_car.view.*
import kotlin.contracts.ExperimentalContracts

class ItemCarView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyleAttribute: Int = 0
) : ConstraintLayout(context, attributeSet, defStyleAttribute)
    , Binder<Car> {

    @ExperimentalContracts
    override fun bind(item: Car?) {

        tv_title.text = item
            .kotlinNullHandler("item car must not be null")
            .title

        tv_dateTime.text = item.dateTime
        tv_ingress.text = item.ingress

        GlideApp.with(this)
            .load(item.image)
            .into(iv_background)

    }

}

