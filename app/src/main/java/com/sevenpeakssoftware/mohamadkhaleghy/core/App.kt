package com.sevenpeakssoftware.mohamadkhaleghy.core

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import com.sevenpeakssoftware.mohamadkhaleghy.core.repo.api.apiModule
import com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db.dbModule
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.carsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        context = this

        startKoin {
            androidContext(this@App)
            modules(listOf(apiModule, dbModule, carsModule))
        }

    }

    companion object {
        //this warning about liking the context of the application can be ignored because we don't have any
        // multi processing in this app and application context is singleton any way.
        @SuppressLint("StaticFieldLeak")
        lateinit var context: Context
    }
}