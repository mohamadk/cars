package com.sevenpeakssoftware.mohamadkhaleghy.core.imageloder

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class SPGlideModule : AppGlideModule() {
}