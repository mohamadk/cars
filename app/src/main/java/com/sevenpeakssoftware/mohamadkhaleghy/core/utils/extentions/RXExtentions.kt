package com.sevenpeakssoftware.mohamadkhaleghy.core.utils.extentions

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import com.mohamadk.middleman.networkstate.NetworkState
import com.sevenpeakssoftware.mohamadkhaleghy.R
import com.sevenpeakssoftware.mohamadkhaleghy.core.App
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.functions.Function
import java.io.IOException
import java.net.ConnectException
import java.net.SocketTimeoutException

fun <T> Observable<T>.toLiveData(strategy: BackpressureStrategy = BackpressureStrategy.LATEST): LiveData<T> {
    return LiveDataReactiveStreams.fromPublisher(toFlowable(strategy))
}

fun handleConnectionErrors(): Function<in Throwable, out NetworkState> {
    return Function { throwable ->
        if (!isInternetAvailable()) {
            NetworkState.error(App.context.getString(R.string.no_internet))
        } else if (throwable is SocketTimeoutException || throwable is IOException) {
            NetworkState.error(App.context.getString(R.string.server_is_not_responding_right_now_please_try_again_later))
        } else if (throwable is ConnectException) {
            NetworkState.error(App.context.getString(R.string.cannot_connect_to_the_server_please_check_you_internet_connection))
        } else {
            NetworkState.error(throwable.message)
        }
    }
}
