package com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Item

class CarsTypeConverters {

    @TypeConverter
    fun tagsToJson(tags: List<String>?): String {
        return Gson().toJson(tags)
    }

    @TypeConverter
    fun jsonToTags(value: String): List<String>? {
        return Gson().fromJson(value, Array<String>::class.java).toList()
    }

    @TypeConverter
    fun itemsToJson(tags: List<Item>?): String {
        return Gson().toJson(tags)
    }

    @TypeConverter
    fun jsonToItems(value: String): List<Item>? {
        return Gson().fromJson(value, Array<Item>::class.java).toList()
    }


}