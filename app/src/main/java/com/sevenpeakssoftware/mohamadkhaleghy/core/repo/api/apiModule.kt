package com.sevenpeakssoftware.mohamadkhaleghy.core.repo.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


const val BASE_URL = "https://www.apphusetreach.no/application/119267/"

val apiModule = module {

    single<Retrofit> {
        val httpUrl = BASE_URL.toHttpUrlOrNull()!!

        Retrofit.Builder()
            .baseUrl(httpUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(get())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
    }

    single<Interceptor>(StringQualifier("logger")) {
        val logger = HttpLoggingInterceptor(HttpLoggingInterceptor.Logger.DEFAULT)
        logger.level = HttpLoggingInterceptor.Level.BODY
        logger
    }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addInterceptor(interceptor = get(StringQualifier("logger")))
            .build()
    }

    single<Gson> {
        GsonBuilder()
            .registerTypeAdapter(Array<Car>::class.java, CarsResponseTypeAdapter())
            .create()
    }
}