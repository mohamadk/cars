package com.sevenpeakssoftware.mohamadkhaleghy.core.repo.api

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import java.lang.reflect.Type

class CarsResponseTypeAdapter : JsonDeserializer<Array<Car>> {
    val gson = Gson()

    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): Array<Car> {

        return gson.fromJson(json.asJsonObject.getAsJsonArray("content"), Array<Car>::class.java)
    }

}
