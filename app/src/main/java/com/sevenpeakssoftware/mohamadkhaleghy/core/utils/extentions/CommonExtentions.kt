package com.sevenpeakssoftware.mohamadkhaleghy.core.utils.extentions

import java.net.InetAddress
import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.contract

@ExperimentalContracts
fun <T> T?.kotlinNullHandler(errorMessage: String): T {
    contract { returns() implies (this@kotlinNullHandler != null) }
    return this ?: throw NullPointerException(errorMessage)
}

fun isInternetAvailable(): Boolean {
    return try {
        val inetAddress = InetAddress.getByName("google.com")
        !inetAddress.equals("")
    } catch (e: Exception) {
        false
    }

}