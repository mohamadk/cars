package com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db.CarsDB.Companion.DB_VERSION
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.item.Car
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.repo.db.CarsDao


@Database(entities = [Car::class], version = DB_VERSION)
@TypeConverters(CarsTypeConverters::class)
abstract class CarsDB : RoomDatabase() {

    abstract val carsDao: CarsDao

    companion object {
        const val DB_VERSION = 1
        const val DB_NAME = "CarsDb"
    }
}