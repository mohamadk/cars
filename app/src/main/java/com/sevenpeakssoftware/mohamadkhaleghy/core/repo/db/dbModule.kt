package com.sevenpeakssoftware.mohamadkhaleghy.core.repo.db

import androidx.room.Room
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dbModule = module {

    single {
        Room.databaseBuilder(androidContext(), CarsDB::class.java, CarsDB.DB_NAME).build()
    }
}