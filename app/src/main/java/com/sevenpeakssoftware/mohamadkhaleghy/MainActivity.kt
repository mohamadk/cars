package com.sevenpeakssoftware.mohamadkhaleghy

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mohamadk.pagingfragment.base.factories.DialogPageFactory
import com.mohamadk.pagingfragment.base.factories.PageFactory
import com.mohamadk.pagingfragment.intractors.FragmentOpener
import com.sevenpeakssoftware.mohamadkhaleghy.pages.cars.CarsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity()
    , FragmentOpener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            open(CarsFragment.CarsPage())
        }

    }

    override fun open(page: DialogPageFactory) {
        // ignored since we don't have dialog in this project
    }

    override fun open(page: PageFactory) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.mainContentLay, page.build(), page.tag())
            .also {
                if (page.addToBackStack()) {
                    it.addToBackStack(page.tag())
                }
            }
            .commit()
    }

    override fun setTitle(title: String?) {
        this.title = title
    }

}
